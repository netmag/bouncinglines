/* Public domain code from ,,The Demo Java programing'' 
   modified by Radim Kolar 1998 (hsn@cybermail.net)/NetMag CZ      
   
see http://ncic.netmag.cz/apps/nase/bouncinglines.html for latest
version.                                                      */

import java.awt.*;
import java.applet.Applet;

public class BouncingLines extends Applet implements Runnable
{
    Thread runner = null;

    Image    image;
    Graphics graphics;

    // bouncing lines member variables

    int[] x1;
    int[] y1;
    int[] x2;
    int[] y2;

    int dx1;
    int dy1;
    int dx2;
    int dy2;

    int first, HEIGHT,WIDTH;

    int LINES, SPEED, FADE, TIMECONTROL;
    
    Color bgcolor;
    
    volatile long lastdrawtime;


    public void init()
    {
      /* bereme taky nejake ty parametre, ne? */
      String s = getParameter("lines");
      if(s==null) LINES=30; else  LINES=Integer.parseInt(s);
      
      s = getParameter("speed");
      if(s==null) SPEED=90; else  SPEED=Integer.parseInt(s);

      s = getParameter("fade");
      if(s!=null)  if(s.equals("yes")) FADE=1; else  FADE=0;

      s = getParameter("timecontrol");
      if(s!=null)  if(s.equals("yes")) TIMECONTROL=1; else  TIMECONTROL=0;
            
      
      s  = getParameter( "bgcolor");


        if( s != null )
            bgcolor=parseColorString(s);
            else
            bgcolor=Color.white;
      
     
        first=0;
        
        /* merime velikost */
        
        WIDTH=size().width;
        HEIGHT=size().height;
        
        dx1 = 2 + (int)(3 * Math.random());
        dy1 = 2 + (int)(3 * Math.random());
        dx2 = 2 + (int)(3 * Math.random());
        dy2 = 2 + (int)(3 * Math.random());


        // create arrays to hold the line coordinates

        x1 = new int[LINES];
        y1 = new int[LINES];
        x2 = new int[LINES];
        y2 = new int[LINES];

        // initialise the first line

        x1[0] = (int)(WIDTH  * Math.random());
        y1[0] = (int)(HEIGHT * Math.random());
        x2[0] = (int)(WIDTH  * Math.random());
        y2[0] = (int)(HEIGHT * Math.random());

        // initialise all the other lines

        for (int i = 1; i < LINES; i++)
        {
            x1[i] = x1[0];
            y1[i] = y1[0];
            x2[i] = x2[0];
            y2[i] = y2[0];
        }
        image    = createImage(WIDTH, HEIGHT);
        graphics = image.getGraphics();
    }


    public void start()
    {
        // user visits the page, create a new thread

        if (runner == null)
        {
            runner = new Thread(this);
            runner.start();
        }
    }

    private Color parseColorString(String colorString)
    {


        if(colorString.length()==6){
            int R = Integer.valueOf(colorString.substring(0,2),16).intValue();
            int G = Integer.valueOf(colorString.substring(2,4),16).intValue();
            int B = Integer.valueOf(colorString.substring(4,6),16).intValue();
            return new Color(R,G,B);
        }
        else return Color.lightGray;
    }

    
    public void stop()
    {
        // user leaves the page, stop the thread

        if (runner != null && runner.isAlive())
            runner.stop();

        runner = null;
    }


    public void run()
    {
        long t;
        while (runner != null)
        {
            repaint();

            try
            {
                if(TIMECONTROL==0)  { Thread.sleep(SPEED);continue;}
                t=System.currentTimeMillis();
                if(t-lastdrawtime<SPEED)
                    Thread.sleep(SPEED-t+lastdrawtime);
            }
            catch (InterruptedException e)
            {
                // do nothing
            }
        }
    }


    public void paint(Graphics g)
    {
        update(g);
    }


    public void update(Graphics g)
    {
        graphics.setColor(bgcolor);
        graphics.fillRect(0, 0, WIDTH, HEIGHT);

        // draw the lines

        graphics.setColor(Color.red);

        int line = first;
        
        Color c;
        for (int i = 0; i < LINES; i++)
        {
            if (FADE==1) c=new Color((float)x1[line]/WIDTH,(float)i/LINES,(float)x2[line]/WIDTH);
            else c=new Color((float)x1[line]/WIDTH,(float)y1[line]/HEIGHT,(float)x2[line]/WIDTH);
            graphics.setColor(c);
            graphics.drawLine(x1[line], y1[line], x2[line], y2[line]);
            line++;
            if (line == LINES) line = 0;
        }

        line = first;

        first--;

        if (first < 0) first = LINES - 1;

        x1[first] = x1[line];
        y1[first] = y1[line];
        x2[first] = x2[line];
        y2[first] = y2[line];

        // move the "first" line

        if (x1[first] + dx1 < WIDTH)  x1[first] += dx1; else dx1 = -(2 + (int)(3 * Math.random()));
        if (x1[first] + dx1 >= 0)     x1[first] += dx1; else dx1 =   2 + (int)(3 * Math.random());
        if (y1[first] + dy1 < HEIGHT) y1[first] += dy1; else dy1 = -(2 + (int)(3 * Math.random()));
        if (y1[first] + dy1 >= 0)     y1[first] += dy1; else dy1 =   2 + (int)(3 * Math.random());
        
        if (x2[first] + dx2 < WIDTH)  x2[first] += dx2; else dx2 = -(2 + (int)(3 * Math.random()));
        if (x2[first] + dx2 >= 0)     x2[first] += dx2; else dx2 =   2 + (int)(3 * Math.random());
        if (y2[first] + dy2 < HEIGHT) y2[first] += dy2; else dy2 = -(2 + (int)(3 * Math.random()));
        if (y2[first] + dy2 >= 0)     y2[first] += dy2; else dy2 =   2 + (int)(3 * Math.random());

        // copy buffer to screen

        g.drawImage(image, 0, 0, this);
        
        lastdrawtime=System.currentTimeMillis();
    }
}
